# Site web et ressources de l'UE Apprentissage Profond Et Image

[La page de l'UE est ici](http://alexandre.meyer.pages.univ-lyon1.fr/m2-apprentissage-profond-image/)




## Explication de la génération

Le site web est désormais fabriqué par ```Hugo``` (thème [congo](https://jpanther.github.io/congo/)). les sources se trouvent dans le répertoire ```web```. Le site web est mis à jour par intégration continue (CI/CD) à chaque fois que vous faites un push (rien besoin d'autre, à part attendre quelques secondes). Le script d'intégration continue est ```.gitlab-ci.yml```.  Pour voir le résultat du script de génération, [allez ici](https://forge.univ-lyon1.fr/Alexandre.Meyer/m2-apprentissage-profond-image/-/jobs) ou depuis l'interface dans CI/Jobs.

Le fichier ```site/config.toml``` permet de configurer la génération du site. Mais noramlement il n'y a pas besoin d'y toucher sauf pour changer les menus et le titre du site.
   * Les pages web sont générées à partir du répertoire ```web/content```. 
   * La page principale du site est ```web/content/_index.html```. Il faut bien laissé le ```_```, il indique qu'il y a des sous-répertoires 
   * ```web/content/am```: les pages de contenus de la partie AM
   * ```web/content/jd```: les pages de contenus de la partie JD
   * ```web/content/nb```: les pages de contenus de la partie NB
   * ```web/static``` : les fichiers autres (pdf, images, etc.) sont à ranger dedans. Par exemple, il y a 
      * ```web/static/images``` pour les images du site;
      * ```web/static/doc``` documents généraux de l'UE;

ATTENTION : quand vous mettez un fichier ```web/content/am/TP_Installation.md``` le lien est sans majuscule !

La doc du générateur "hugo" : [https://gohugo.io/content-management/organization/](https://gohugo.io/content-management/organization/)


## Tester le site en local
Pour tester vos mises à jour en local :
   * installer hugo : ```sudo apt install hugo```
   * dans le répertoire web, faire ```hugo serve```
   * dans votre navigateur, entrez l'url ```localhost:8000```

Vous pouvez aussi essayer de contruire le site en static en faisant juste ```hugo``` : le site sera constuit dans le répertoire public.


### D'autres infos
Pour convertir du DOKUWIKI en Markdown, on peut utiliser pandoc??? (todo) ou certains web en ligne.