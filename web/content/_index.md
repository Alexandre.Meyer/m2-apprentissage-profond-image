# Master 2 ID3D - Analyse, Traitement d'Image et Vision 3D
# Master 2 IA - Apprentissage Machine Et Image

Teachers: [Alexandre Meyer](http://liris.cnrs.fr/alexandre.meyer), [Julie Digne](http://liris.cnrs.fr/jule.digne) et [Nicolas Bonneel](http://liris.cnrs.fr/nicolas.bonneel) - LIRIS, Université Claude Bernard Lyon 1

3 ects: 30h (CM/TP)


![MLImage_all.jpg](images/MLImage_all.jpg)

## Objective
<p style="text-align:justify;"> This page contains material for the course 'Learning and Image' in the Master of Computer Science at the University of Lyon 1 (ATIV3D for ID3D, and ‘Learning and Image’ for IA). The course takes place in autumn. The aim of the course is to provide an overview in machine learning (particularly deep learning) for image problems. The course begins by presenting the classic image-related problems, such as classification, descriptor extraction, pattern recognition, object tracking, segmentation, etc. Then, it presents generative methods. A wide range of different types of networks (CNN, auto-encoder, LSTM, GAN, transform, diffusion,etc.) is given, focusing on image data, but also on data such as point clouds, meshes, animation (skeleton), colour palettes, etc.
</p>

[For the IA Master's options, the slides are here.](doc/MLImage_PresOption.pdf)


## Topics

### Deep learning and images
  * Basis:  training, latent space, regularization, etc.
  * ConvolutionNN
  * Segmentation (U-Net, etc.)
  * Tracking (YOLO)
  * Skeleton (OpenPose, XNect, etc.)
  * Notion of transformer/attention for classification

### Generative deep learning
  * Images generation
    * auto-encoder
    * GAN
    * Diffusion
  * Extended to 3D data
  
### Deep learning and geometry
  * Geometric data
     * Point cloud (pointNet, etc.)
     * Meshes (MeshConv, etc.)
     * Diffusion on surface
  * Implicit neuronal representation (IGR, SIREN)
  * Neural radiance field NERF (Champs de radiance neuronaux)

### Optimal transport
  * Introduction to optimal transport
  


## Timetable: autumn 2024

For the IA Master's programme, classes and practical work are Thursday afternoons between October to January. For ID3D, there are classes on Tuesday mornings in September, then in October the lecturers are on Thursday afternoons and the TP on Tuesday mornings.


![MLImage_all.jpg](images/2024edt.png)




## Evaluations

* Part NB : evaluation of the TP
* Part AM : exam + ~ evaluation of the TP (see bellow)
* Part JD : exam

### TP PartAM

The TP "Synthesis of the image of a person guided by a posture" is optinal. If you want to be marked, it will count as 50% of your AM grade. If you don't want to be marked, the grade for the AM part will be the exam only. **This TP can be done alone or in pairs.**


**Dealine: Friday 15th November**. You will upload a ZIP file containing all your files in TOMUSS including the trained network. No report, but the README.md must contain all the informations:

* how to run the code using the trained network; 
* how to train the networks
* and what you did exactly explain in English or French.
There will certainly be a 5-minute demo during december.