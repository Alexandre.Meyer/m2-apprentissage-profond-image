---
title: "Vision, image and machine learning (partie AM)"
description: "Partie A. Meyer"
---





## The foundations of Deep Learning for images
  * [Alexandre Meyer](https://perso.liris.cnrs.fr/alexandre.meyer)
<!---
  * [L'ancienne page de cette partie](https://perso.liris.cnrs.fr/alexandre.meyer/public_html/www/doku.php?id=image_deeplearning)
-->


![Image alt](images/dl_am.jpg)
  

### Le Cours

* [CM1](doc/DLIM-CM1_NN.pdf): Introduction to neural networks (forward and backpropagation)
* [CM2](doc/DLIM-CM2_CNN.pdf): Convolution Neural Networks (CNN)
* [CM3](doc/DLIM-CM3_TowardGeneration.pdf): Auto-encoder (AE), processing skeleton data, introduction to generative approaches (basic GAN)
* [CM4](doc/DLIM-CM4_Vision.pdf): the "modern" vision (Segmentation, Tracking-YOLO, Transformer for Images)


  

<!---
-   La vidéo du CM de la 1ère partie : TODO
-   La vidéo du CM de la 2e partie
<iframe width="560" height="315" src="//www.youtube.com/embed/ge7V2C7eVWk" frameborder="0" allowfullscreen></iframe>
-->
  
  

  

### Tutorials (TP)

0. [Installation](tp_installation)
1. [A neural network from scratch](tp_nnfromscratch).  (Python, numpy)
2. Classification
   1. [2D Point Classification](tp_classificationpoint). (Pytorch)   (If you follow the Master's Course, go directly to question 2.2 or 3)
   2. [Image classification using Convolution Neural Networks](tp_classificationcnn). (CNN, Pytorch)
3. [Style transfer between images](tp_style). (Pytorch)
4. [Gesture transfer and person image generation](tp_dance)

* [Old TP: autoencoder and animation](tp_aeanimation)
