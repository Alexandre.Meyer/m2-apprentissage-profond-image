---
title: "Vision, image and machine learning (partie AM) - TP"
description: "Partie A. Meyer"
---


![Image alt](../images/forward_backward.png)



## Tutorial (TP)

The aim of this tutorial is to implement from scratch a simple neural network trained by backpropagation. This will be a formative introduction to more advanced frameworks (such as PyTorch) where learning is automated.

[All steps are described in the start code here.](https://raw.githubusercontent.com/ucacaxm/DeepLearning_Vision_SimpleExamples/refs/heads/master/src/dl_from_scratch/DLNetworkFromScratch_EXO.py)


## Documents to read

In addition to the course slides:
* [A simple NN 2D->2D](https://github.com/ucacaxm/DeepLearning_Vision_SimpleExamples/blob/master/src/dl_from_scratch/DLNetworkUltraSimple2dto2D.py)
* [The equations behind backpropagation with code and explanations](http://neuralnetworksanddeeplearning.com/chap2.html#the_four_fundamental_equations_behind_backpropagation)
* [The backpropagation equations](doc/backpropagation-from-scratch.pdf)
* [Introduction-au-deep-learning(Miximum)](https://www.miximum.fr/blog/introduction-au-deep-learning-2/)           


## Solutions
* [Correction](https://raw.githubusercontent.com/ucacaxm/DeepLearning_Vision_SimpleExamples/refs/heads/master/src/dl_from_scratch/DLNetworkFromScratch.py)

Variant
* [From Miximum](https://raw.githubusercontent.com/ucacaxm/DeepLearning_Vision_SimpleExamples/refs/heads/master/src/dl_from_scratch/DLNetworkFromScratch_WEB1.py)
