---
title: "Vision, image and machine learning (partie AM)"
description: "Partie A. Meyer"
---

# Installation


## Les outils de dév en local

Nous vous conseillons de faire l'installations de Anaconda. Anaconda est un gestionnaire d'environnements Python. Il vous permet entre autre d'avoir plusieurs installations de Python avec des packages différents sans interférence entre chaque environnement. Si vous avez besoin d'un turorial décrivant l'installation de chaque étape en détails, [regardez ici pour Ubunu](https://docs.anaconda.com/anaconda/install/linux/) et [ici pour Windows](https://docs.anaconda.com/anaconda/install/windows/). 

```
conda create --name deepan python=3.8
```

Dans un environnement python, il faut installer *Numpy, MatPlotlib, OpenCV et PyTorch*. Pour installer PyTorch et de nombreuses dépendances, regardez [la ligne de commande que propose PyTorch](https://pytorch.org/get-started/locally/). Puis installer OpenCV et Mediapipe avec 
```
pip install mediapipe
```
Mediapipe installe OpenCV. Si vous avez installé OpenCV avant, parfois Mediapipe ne marche pas.


Sous Windows lancez *anaconda prompt*, sous Linux ouvrez un terminal, puis lancez ```conda activate p36```

Votre code va être écrit en Python. Vous pouvez utiliser [VisualCode](https://code.visualstudio.com/docs/languages/python) par exemple ou [l'IDE Spyder](https://www.spyder-ide.org/) qui s'installe avec Anaconda ou [PyCharm](https://www.jetbrains.com/pycharm/) qui est gratuit pour les étudiants. Éventuellement écrire votre code dans un éditeur de code puis lancer votre script comme ceci :
``` python mon_prog.py```

 
## Colab

![Image alt](../images/colab.jpg)

Ou vous pouvez travailler avec [Colab/Google](https://colab.research.google.com/). Vous trouverez [un tutoriel ici sur medium](https://medium.com/deep-learning-turkey/google-colab-free-gpu-tutorial-e113627b9f5d). Colab permet d'écrire et exécuter du code en ligne, possibilité de le
faire tourner gratuitement sur une Nvidia K80.
