---
title: "Apprentissage et images (partie AM) - TP"
description: "Partie A. Meyer"
---


## Partie (IV) Animation et DL
<!---
Une vidéo courte expliquant quelques principes pour le TP :
<iframe width="560" height="315" src="//www.youtube.com/embed/ZXjhquKAfVs" frameborder="0" allowfullscreen></iframe>
-->
  
  

* [Fast Neural Style Transfer for Motion Data](http://www.ipab.inf.ed.ac.uk/cgvu/cga2017.pdf), Holden etal, 2017.

Le papier qui propose une approche équivalente pour transférer le style d'une animation vers une autre. Le contenu est le geste et le style est l'effet donné dans le geste en relation avec l'état émotionnel, la personnalité ou les caractéristiques physiques particulières (ages, force, etc.). Ce Tp se propose de coder une version simplifiée de ce papier. Nous ferons, comme pour les images précédemment, une optimisation. Le papier original propose d'entrainer un réseau à faire ce travail, ce qui serait plus efficace une fois le réseau entrainé mais qui demanderait plusieurs heures de calculs. Pour ce TP l'optimisation ne prendra que quelques minutes, mais ne fournira un transfert qu'entre 2 animations.

  

### Le code  
 * [Le code de départ est à télécharger ici.](https://perso.liris.cnrs.fr/alexandre.meyer/teaching/master_charanim/download/StyleTransfer.zip)
 * [le code](../doc/tp_style_transfer.zip)


Il faut installer un peu plus de lib que pour pytorch. Panda3D, pyglm, etc. sont nécessaires pour la visualisation des animations. Vous pouvez sûrement pouvoir installer un env avec le fichier .yml fournit
```
    conda env create -f environment.yml
```

Ou alors un environnement neuf :
```
    conda create --name py-deepan python=3.8
    conda activate py-deepan
    conda install numpy
    conda install pillow
    conda install pytorch torchvision torchaudio cudatoolkit=11.3 -c pytorch    (mais allez voir la page de pytorch)
    conda install multipledispatch
    pip install panda3d                   (==1.10.7)
    pip install torchsummary
    pip install PyGLM
```

  

* Vous devez installer le code comme un package pour que les 'import' trouve les fichiers :
```
    cd ....StyleTransfer
    pip install -e .
```

  

* En cas de problème pour trouver les données=le chemin vers data (et seulement en cas de problème). Il faut modifier dans ```pydeepan/__init__.py``` la variable tout à la fin après le dernier "else":
```
    else:
       dir_resources = dir_pydeepan+"/data"                      # mettez le chemin absolu???
```

  
* Le programme principal à lancer est
```
    **pydeepan/demo/StyleTransferDemo.py**.
```
 * 'z' (dés)active l'animation
 * 'n' : joue la frame suivante d'une animation
 * 'b' : joue la frame précédente d'une animation
 * 'p' : passe aux animations suivantes (en restant dans le même dataset)
 * 'd' : change de dataset (il y en a deux : StyleTransfer et Emilya)
 * 'l' : lance l'optimisation sur les longueurs de membres
 * 'e' : passe les animations dans l'auto-encodeur
 * 't' : transfert le style d'une animation vers une autre (le code est à compléter)

  

Une animation est représentée par un tableau de 240 frames x 73 valeurs. Voir le fichier HPAAnimation.py pour une descriptions valeurs de 0 à 65 qui représentent les positions 3D de chaque articulation. Il n'y a pas de notion d'angles dans ce format d'animations. Déplacer une articulation revient souvent à changer la longueur d'un membre. Il y a une optimisation (touche 'l' et class AOSkeletonConstraint.py) qui effectue une descente de gradient (pytorch) pour "remettre" les longueurs de membre aux valeurs initiales.

Remarque : l'autoencoder de ce code est très basique. Il introduit des tremblements pour certaines animations. Il y a surement moyen de faire mieux.

  
  

### Tester l'auto-encodeur

Ajouter une action liée à la touche 'r' qui "casse" une animation pour une ou deux articulations (par exemple figer épaule+coude). Puis vous pourrez tester le passage dans l'auto-encoder avec la touche 'e' (e=encodeur). Testez également l'optimisation sur les longueurs de membres. Regardez dans le fichier "HPAAnimation.py" pour une description des articulations.
```
          # 42 = coord X de l'épaule gauche. ':' signifie toutes les valeurs. np.mean calcule la moyenne.
          self.anims[:,42] = np.mean( self.anims[:,42] )+ 2   
```
Vous pouvez également ajouter de l'aléatoire sur certaines articulations. Pas sur toutes en même temps, l'auto-encodeur n'est pas si fort.

  
  

### Interpolation dans l'espace caché (espace latent)

* Faites l'interpolation entre l'animation 1 et l'animation 2 dans l'espace 3D, ranger le résultat dans l'animation 3.
* Faites l'interpolation entre l'animation 1 et l'animation 2 dans l'espace latent (l'espace de l'ato-encodeur), ranger le résultat dans l'animation 4.
* Comparez les 2 résultats.

  

### Transfert de style

Comme pour les images, l'objectif est de produire une nouvelle animation par optimisation de toutes les positions pour toutes les frames. Donc d'optimiser les 240x73 valeurs. Le code à trou réalisant l'optimisation pour transférer le style est dans **pydeepan/chara/AOStyleTransfer.py**. Vous devez compléter les fonctions 'optimize', 'loss' et 'gram'.

  

* Essayer déjà de partir d'un tableau/animation random est d'optimiser pour produire une animation ayant le même code que l'animation source (sans s'occuper du style). Dans la fonction 'loss', encoder les deux animations et l'erreur sera la différence au carré des deux codes. Il faut jouer sur le 'learning rate'. A la fin, vous obtenez deux animations très différentes mais qui ont le même code. Un passage dans l'auto-encodeur réduit fortement les différences.

* Ajouter maintenant les infos de style de la 2e animation source dans la fonction 'loss'.

  
  
  

  

  
